//SOUNDS
var audio = [];

//TIMEOUTS
var timeout = [],
	timer, 
	launch = [],
	sprite = [],
	globalName,
	tincan,
	typingText;

//AWARDs NUMBER
var blazonAward = [];
	blazonAward["aktobe"] = 0,
	blazonAward["kostanai"] = 0,
	blazonAward["east-kazakhstan"] = 0,
	blazonAward["karaganda"] = 0; 

var blazons;

var blazonPlusOne = function(blazonName)
{
	var currBlazon = $(".blazon-" + blazonName),
		currBlazonPic = $(".blazon-" + blazonName + " .pic"),
		currBlazonText = $(".blazon-" + blazonName + " .text");
		
	currBlazonPic.addClass("pic-shadowed");
	blazonAward[blazonName] ++;
	currBlazonText.html(blazonAward[blazonName]);
	
	timeout[0] = setTimeout(function(){
		currBlazonPic.removeClass("pic-shadowed");
	}, 2000);
}
	
var startTimer = function(jqueryElement, secondsLeft, callBack)
{
	if(secondsLeft <= 0)
		callBack();
	else
	{
		secondsLeft --;
		var minutesLeft = Math.floor(secondsLeft / 60);
		var result = "";
		secondsLeft = Math.round(secondsLeft % 60);
		
		if(minutesLeft > 9) 
			result = "" + minutesLeft + ":";
		else
			result = "0" + minutesLeft + ":";
		
		if(secondsLeft > 9)
			result += secondsLeft;
		else 
			result += "0" + secondsLeft;
		
		jqueryElement.html(result);
		
		secondsLeft += (minutesLeft * 60);
		
		timer = setTimeout(function(){
			startTimer(jqueryElement, secondsLeft, callBack);
		}, 1000);
	}
}

var stopTimer = function()
{
	clearTimeout(timer);
}

var posAndSize = function(left, top, width, height)
{
	var pageW = 2000,
		pageH = 1000,
		leftP = left / 2000 * 100, 
		topP = top / 1000 * 100, 
		widthP = width / 2000 * 100,
		heightP = height / 1000 * 100;
		
	var css = {
		"left": leftP + "%",
		"top": topP + "%",
		"width": widthP + "%",
		"height": heightP + "%"
	};
	
	return css;
}
	
var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

var fadeOneByOne2 = function(jqueryElement, curr, interval, endFunction) //THE PREV ELEMENT FADES OUT
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			if(curr > 0)
				$(jqueryElement[curr - 1]).fadeOut(0);
			fadeOneByOne2(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function sardorAudio(currAudio, nextAudio)
{
	this.currAudio = currAudio;
	this.nextAudio = nextAudio;
	this.currAudio.addEventListener("ended", function(){
		nextAudio.play();
	});
}

sardorAudio.prototype.play = function()
{
	this.currAudio.play();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
		this.play();
	});

	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if(tObj.text[tObj.currentSymbol] === "<")
		{
			var numToPass = tObj.currentSymbol + 1, 
				openTag = "<",
				text = "", 
				closeTag = "";
				
			while(tObj.text[numToPass] !== ">")
			{
				openTag += tObj.text[numToPass];
				numToPass ++;
			}
			
			openTag += ">";
			
			numToPass ++;
			
			if(openTag === "<br>")
				tObj.jqueryElement.append(openTag);
			else
			{
				while(tObj.text[numToPass] !== "<")
				{
					text += tObj.text[numToPass];
					numToPass ++;
				}
				
				while(tObj.text[numToPass] !== ">")
				{
					closeTag += tObj.text[numToPass];
					numToPass ++;
				}
				
				closeTag += ">";
				numToPass ++;

				tObj.jqueryElement.append(openTag + text + closeTag);
			}
			
			tObj.currentSymbol = numToPass;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
		{
			tObj.write();
		}
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
	this.jqueryElement.html(this.text);
}

var matches = function(ch, exp){
	for(var i = 0; i < exp.length; i++)
		if(exp[i] === ch)
			return true;
	
	return false;
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum);
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, color, interval, times, callBack)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("background-color", color);
		timeout[1] = setTimeout(function(){
			jqueryElements.css("background-color", "");
			if (times) 
				blink(jqueryElements, color, interval, --times, callBack)		
			else
				callBack();
		}, intervalHalf);
	}, intervalHalf);
}

var blink2 = function(jqueryElements, interval, times, callBack)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.addClass("blink-shadow");
		timeout[1] = setTimeout(function(){
			jqueryElements.removeClass("blink-shadow");
			if (times) 
				blink2(jqueryElements, interval, --times, callBack)		
			else
				callBack();
		}, intervalHalf);
	}, intervalHalf);
}

var isCorrect = function(jqueryElement)
{
	return parseInt(jqueryElement.html()) === parseInt(jqueryElement.attr("data-correct"));
}

var setLRSData = function(){
	tincan = new TinCan (
    {
        recordStores: [
            {
                "endpoint": "http://54.154.57.220/data/xAPI/",
				"username": "e083498f4e256e68ab2c5ae2be4195d9a348eb20",
				"password": "c6ea3c32a9d77919d0eb3cdea3bc5d460f50d93b"
            }
        ]
    });
}

var sendLaunchedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/launched"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var sendCompletedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz/chemistry"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/completed"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var setMenuStuff = function()
{
	var enterButton = $(".enter-button"),
		annotation = $("#frame-000 .annotation-hidden"),
		annotationButton = $("#frame-000 .annotation-button"),
		password = $(".password"),
		error = $(".error"),
		name = $(".name");
		name.val("");
		password.val("");
		
		
		sayHello = function(){
		regBox.html("Здравствуйте, " + name.val() + "!");
		regBox.css("height", "3em");
		regBox.css("padding", "0.5em");
		regBox.css("text-align", "center");
		globalName = name.val();
		regBox.addClass("topcorner");
		timeout[0] = setTimeout(function(){
			regBox.html(name.val());
			regBox.css("width", name.val().length + "em");
		}, 3000);
	};
	
	if(globalName)
		sayHello();
	
	var annotationButtonListener = function(){
		annotation.toggleClass("annotation-shown");
		annotationButton.toggleClass("annotation-button-close");
	};
	annotationButton.off("click", annotationButtonListener);
	annotationButton.on("click", annotationButtonListener);
	
	var enterButtonListener = function(){
		if(password.val() === "12345")
			sayHello();
		else
			error.html("неверный пароль");
	};
	enterButton.off("click", enterButtonListener);
	enterButton.on("click", enterButtonListener);
}

launch["frame-000"] = function()
{
}

launch["frame-101"] = function()
{
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		flags = $(prefix + ".flags"),
		flagsHighlighted = $(prefix + ".flags-highlighted");
	
	audio[0] = new Audio("audio/s1.mp3");
		
	fadeNavsOut();
	fadeLauncherIn();
	flags.fadeOut(0);
	flagsHighlighted.fadeOut(0);
	
	startButtonListener = function(){
		audio[0].play();
		timeout[1] = setTimeout(function(){
			flags.fadeIn(1000);
		}, 10000);
		timeout[2] = setTimeout(function(){
			flags.fadeOut(1000);
			flagsHighlighted.fadeIn(1000);
		}, 12000);
		timeout[3] = setTimeout(function(){
			flags.fadeIn(1000);
			flagsHighlighted.fadeOut(1000);
		}, 13000);
		timeout[4] = setTimeout(function(){
			flags.fadeOut(1000);
			flagsHighlighted.fadeIn(1000);
		}, 14000);
		timeout[5] = setTimeout(function(){
			flags.fadeIn(1000);
			flagsHighlighted.fadeOut(1000);
		}, 15000);
		timeout[6] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			hideEverythingBut($("#frame-102"));
			sendCompletedStatement(1);
		}, 23000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 2000);
}

launch["frame-102"] = function()
{
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		aktobeFlag = $(prefix + ".aktobe-flag"),
		aktobeHighlighted = $(prefix + ".aktobe-highlighted"),
		aktobeHelicopter = $(prefix + ".aktobe-helicopter"),
		helicopter = $(prefix + ".helicopter"),
		vocabulary = $(prefix + ".vocabulary"),
		isotope = $(prefix + ".isotope"),
		atomicMass = $(prefix + ".atomic-mass"),
		molarMass = $(prefix + ".molar-mass"),
		ammonia = $(prefix + ".ammonia"),
		glass = $(prefix + ".glass");
	
	audio[0] = new Audio("audio/s2.mp3");
	audio[1] = new Audio("audio/helicopter-sound.mp3");
	audio[2] = new Audio("audio/s3.mp3");
	
	sprite[0] = new Motio(aktobeHelicopter[0], {
		"fps": "3",
		"frames": "12"
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
		
	fadeNavsOut();
	fadeLauncherIn();
	aktobeFlag.fadeOut(0);
	aktobeHighlighted.fadeOut(0);
	aktobeHelicopter.fadeOut(0);
	helicopter.fadeOut(0);
	
	startButtonListener = function(){
		audio[0].play();
				
		timeout[1] = setTimeout(function(){
			helicopter.fadeIn(1000);
		}, 3000);
		timeout[2] = setTimeout(function(){
			aktobeFlag.fadeIn(1000);
		}, 25000);
		timeout[3] = setTimeout(function(){
			aktobeFlag.fadeOut(500);
			aktobeHighlighted.fadeIn(1000);
		}, 28000);
		timeout[4] = setTimeout(function(){
			helicopter.fadeOut(1000);
			aktobeHelicopter.fadeIn(1000);
			audio[1].play();
			sprite[0].play();
		}, 29000);
		timeout[5] = setTimeout(function(){
			aktobeHelicopter.fadeOut(1000);
			isotope.removeClass("out-of-view");
			audio[2].play();
		}, 33000);
		timeout[6] = setTimeout(function(){
			aktobeHelicopter.fadeOut(1000);
		}, 39000);
		timeout[7] = setTimeout(function(){
			atomicMass.removeClass("out-of-view");
		}, 42000);
		timeout[8] = setTimeout(function(){
			molarMass.removeClass("out-of-view");
		}, 48000);
		timeout[9] = setTimeout(function(){
			glass.removeClass("out-of-view");
		}, 55000);
		timeout[10] = setTimeout(function(){
			ammonia.removeClass("out-of-view");
		}, 58000);
		timeout[11] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(2);
		}, 63000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(2);
	}, 2000);
}

launch["frame-103"] = function()
{
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
		var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		aktobeFlag = $(prefix + ".aktobe-flag"),
		aktobeHighlighted = $(prefix + ".aktobe-highlighted"),
		aktobeHelicopter = $(prefix + ".aktobe-helicopter"),
		helicopter = $(prefix + ".helicopter"),
		vocabulary = $(prefix + ".vocabulary"),
		isotope = $(prefix + ".isotope"),
		atomicMass = $(prefix + ".atomic-mass"),
		molarMass = $(prefix + ".molar-mass"),
		ammonia = $(prefix + ".ammonia"),
		glass = $(prefix + ".glass");
	
	audio[0] = new Audio("");
	audio[1] = new Audio("audio/helicopter-sound.mp3");
	audio[1].volume = 0.4;
	
	sprite[0] = new Motio(aktobeHelicopter[0], {
		"fps": "3",
		"frames": "12"
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
		
	fadeNavsOut();
	fadeLauncherIn();
	aktobeFlag.fadeOut(0);
	aktobeHighlighted.fadeOut(0);
	aktobeHelicopter.fadeOut(0);
	helicopter.fadeOut(0);
	
	startButtonListener = function(){
		audio[0].play();
		helicopter.fadeIn(0);
		
		aktobeFlag.fadeIn(1000);
		aktobeHighlighted.fadeIn(1000);
		
		timeout[1] = setTimeout(function(){
			helicopter.fadeOut(1000);
			aktobeHelicopter.fadeIn(1000);
			audio[1].play();
			sprite[0].play();
		}, 2000);
		timeout[2] = setTimeout(function(){
			isotope.removeClass("out-of-view");
		}, 5000);
		timeout[3] = setTimeout(function(){
			aktobeHelicopter.fadeOut(1000);
		}, 6000);
		timeout[4] = setTimeout(function(){
			atomicMass.removeClass("out-of-view");
		}, 9000);
		timeout[5] = setTimeout(function(){
			molarMass.removeClass("out-of-view");
		}, 15000);
		timeout[6] = setTimeout(function(){
			glass.removeClass("out-of-view");
		}, 22000);
		timeout[7] = setTimeout(function(){
			ammonia.removeClass("out-of-view");
		}, 25000);
		timeout[8] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(3);
		}, 30000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(3);
	}, 2000);
}

launch["frame-104"] = function()
{
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		textfields = $(prefix + ".data input"),
		checkButton = $(prefix + ".check-button"),
		bulb = $(prefix + ".bulb"),
		mendeleevTable = $(prefix + ".mendeleev-table"),
		correctNum = 0,
		award = $(".award");
	
	fadeNavsOut();
	fadeLauncherIn();
	textfields.val("");
	checkButton.fadeOut(0);
	award.fadeOut(0);
	
	var bulbListener = function(){
		mendeleevTable.toggleClass("mendeleev-table-hidden");
	};
	bulb.off("click", bulbListener);
	bulb.on("click", bulbListener);
	
	var areDirty = function()
	{
		for (var i = 0; i < textfields.length; i++)
			if($(textfields[i]).val() === "")
				return false;
			
		return true;
	}
	
	var textfieldsListener = function(){
		var currField = $(this);
		var exp = ["0","1","2","3","4","5","6","7","8","9"];
		var currValue = currField.val();
		
		if(matches(currValue.charAt(currValue.length - 1), exp))
		{
			$(prefix + "." + currField.attr("data-key")).html(currValue);
		}
		else
		{
			var currValueArray = currValue.split("");
			currValueArray.pop();
			currValue = currValueArray.join("");
			currField.val(currValue);
		}
		if(areDirty())
			checkButton.fadeIn(300);
	};
	textfields.off("keyup", textfieldsListener);
	textfields.on("keyup", textfieldsListener);
	
	var checkButtonListener = function(){
		for(var i = 0; i < textfields.length; i++)
		{
			var currfield = $(textfields[i]);
			if(currfield.val() === currfield.attr("data-correct"))
			{
				currfield.css("background-color", "#23b23b");
				correctNum ++;
			}
			else
				currfield.css("background-color", "#FF0100");
		}
		timeout[0] = setTimeout(function(){
			for(var i = 0; i < textfields.length; i++)
			{
				var currfield = $(textfields[i]);
				currfield.val(currfield.attr("data-correct"));
				currfield.css("background-color", "");
				currfield.css("color", "green");
			}
			if(correctNum === 4)
			{
				award.fadeIn(1000);
				blazons.fadeIn(500);
				blazonPlusOne("aktobe");
			}
		}, 3000);
		timeout[1] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(4);
		}, 7000);
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
	
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(4);
	}, 2000);
}

launch["frame-105"] = function()
{
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		text = $(prefix + ".text"),
		submarine = $(prefix + ".submarine");
	
	fadeNavsOut();
	fadeLauncherIn();
	submarine.fadeOut(0);
	
	typingText = new TypingText(text, 50, true, function(){});
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			typingText.write();
		}, 1000);
		timeout[2] = setTimeout(function(){
			submarine.fadeIn(1000);
		}, 18000);
		timeout[3] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(5);
		}, 22000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(5);
	}, 2000);
}

launch["frame-106"] = function()
{
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		bulb = $(prefix + ".bulb"),
		mendeleevTable = $(prefix + ".mendeleev-table"),
		textfields = $(prefix + ".task input"),
		checkButton = $(prefix + ".check-button"),
		award = $(prefix + ".award"), 
		correctNum = 0;
	
	fadeNavsOut();
	fadeLauncherIn();
	textfields.val("");
	checkButton.fadeOut(0);
	award.fadeOut(0);
	
	audio[0] = new Audio("audio/s7.mp3");
	
	var bulbListener = function(){
		mendeleevTable.toggleClass("mendeleev-table-hidden");
	};
	bulb.off("click", bulbListener);
	bulb.on("click", bulbListener);
	
	var areDirty = function()
	{
		for (var i = 0; i < textfields.length; i++)
			if($(textfields[i]).val() === "")
				return false;
			
		return true;
	}
	
	var textfieldsListener = function(){
		currField = $(this);
		var exp = ["0","1","2","3","4","5","6","7","8","9",",","."];
		var currValue = currField.val();
		
		if(matches(currValue.charAt(currValue.length - 1), exp))
		{
			$(prefix + "." + currField.attr("data-key")).html(currValue);
		}
		else
		{
			var currValueArray = currValue.split("");
			currValueArray.pop();
			currValue = currValueArray.join("");
			currField.val(currValue);
		}
		
		if(areDirty())
			checkButton.fadeIn(300);
	};
	textfields.off("keyup", textfieldsListener);
	textfields.on("keyup", textfieldsListener);
	
	var checkButtonListener = function(){
		for(var i = 0; i < textfields.length; i++)
		{
			var currfield = $(textfields[i]), 
				currfieldValue = currfield.val();
			currfieldValue = currfieldValue.replace(".", ",");
			
			if(currfieldValue === currfield.attr("data-correct"))
			{
				currfield.css("background-color", "#23b23b");
				correctNum++;
			}
			else
				currfield.css("background-color", "#FF0100");
		}
		timeout[0] = setTimeout(function(){
			for(var i = 0; i < textfields.length; i++)
			{
				var currfield = $(textfields[i]);
				currfield.val(currfield.attr("data-correct"));
				currfield.css("background-color", "");
				currfield.css("color", "green");
			}
		}, 3000);
		timeout[1] = setTimeout(function(){
			if(correctNum === 4)
			{
				award.fadeIn(1000);
				blazons.fadeIn(500);
				blazonPlusOne("aktobe");
			}
		}, 5000);
		timeout[2] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(6);
		}, 7000);
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			audio[0].play();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(6);
	}, 2000);
}

launch["frame-107"] = function()
{
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		bgBlazon = $(prefix + ".bg-blazon"),
		textPicContainer = $(prefix + ".text-pic-container"),
		text = $(prefix + ".text"),
		elems = $(prefix + ".elem"), 
		pb = $(prefix + ".pb"), 
		zn = $(prefix + ".zn"),
		cu = $(prefix + ".cu"),
		au = $(prefix + ".au"),
		ag = $(prefix + ".ag"),
		cd = $(prefix + ".cd"),
		bi = $(prefix + ".bi"),
		sn = $(prefix + ".sn"),
		inn = $(prefix + ".in"),
		ga = $(prefix + ".ga"),
		text = $(prefix + ".text"),
		row1 = $(prefix + ".row-1");
	
	fadeNavsOut();
	fadeLauncherIn();
	elems.fadeOut(0);
	textPicContainer.fadeOut(0);
	
	typingText = new TypingText(text, 50, true, function(){
		row1.toggleClass("highlighted");
		timeout[12] = setTimeout(function(){
			row1.toggleClass("highlighted");
		}, 2000);
		timeout[13] = setTimeout(function(){
			row1.toggleClass("highlighted");
		}, 4000);
		timeout[14] = setTimeout(function(){
			row1.toggleClass("highlighted");
		}, 6000);
	});
	
	audio[0] = new Audio("audio/s8.mp3");
	
	var row1Listener = function(){
		$(this).children().filter(".comment").toggleClass("comment-hidden");
		if(!$(prefix + ".comment-hidden").length)
		{
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(7);
		}
	}
	row1.off("click", row1Listener);
	row1.on("click", row1Listener);
	
	startButtonListener = function(){
		audio[0].play();
		
		timeout[1] = setTimeout(function(){
			bgBlazon.fadeOut(500);
			textPicContainer.fadeIn(500);
		}, 7000);
		timeout[2] = setTimeout(function(){
			pb.fadeIn(500);
		}, 10000);
		timeout[3] = setTimeout(function(){
			zn.fadeIn(500);
		}, 11000);
		timeout[4] = setTimeout(function(){
			cu.fadeIn(500);
		}, 15000);
		timeout[5] = setTimeout(function(){
			au.fadeIn(500);
		}, 16000);
		timeout[6] = setTimeout(function(){
			ag.fadeIn(500);
		}, 17000);
		timeout[7] = setTimeout(function(){
			cd.fadeIn(500);
		}, 18000);
		timeout[8] = setTimeout(function(){
			bi.fadeIn(500);
		}, 19000);
		timeout[9] = setTimeout(function(){
			sn.fadeIn(500);
		}, 20000);
		timeout[10] = setTimeout(function(){
			inn.fadeIn(500);
		}, 21000);
		timeout[11] = setTimeout(function(){
			ga.fadeIn(500);
		}, 22000);
		timeout[12] = setTimeout(function(){
			typingText.write();
		}, 30000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(7);
	}, 2000);
}

launch["frame-108"] = function()
{
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		taskContainer = $(prefix + ".task-container"),
		checkButton = $(prefix + ".check-button"),
		answers = $(prefix + ".answer"),
		award = $(prefix + ".award"),
		correctNum = 0;
	
	fadeNavsOut();
	fadeLauncherIn();
	taskContainer.fadeOut(0);
	checkButton.fadeOut(0);
	award.fadeOut(0);
	
	var answersListener = function(){
		$(this).addClass("selected");
		$(this).siblings().removeClass("selected");
		
		if($(prefix + ".selected").length === 3)
			checkButton.fadeIn(500);
	};
	answers.off("click", answersListener);
	answers.on("click", answersListener);
	
	var checkButtonListener = function(){
		var selecteds = $(prefix + ".selected");
		
		for(var i = 0; i < selecteds.length; i++)
		{
			var currElem = $(selecteds[i]);
			if(currElem.hasClass("true"))
			{
				currElem.css("color", "green");
				correctNum++;
			}
			else
				currElem.css("color", "red");
		}
		
		timeout[0] = setTimeout(function(){
			selecteds.css("color", "");
			selecteds.removeClass("selected");
			
			$(prefix + ".true").css("color", "green");
		}, 4000);
		timeout[1] = setTimeout(function(){
			if(correctNum === 3)
			{
				award.fadeIn(500);
				blazons.fadeIn(500);
				blazonPlusOne("east-kazakhstan");
			}
		}, 5000);
		timeout[2] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(8);
		}, 7000);
	};
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	startButtonListener = function(){
		taskContainer.fadeIn(500);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(8);
	}, 2000);
}

launch["frame-109"] = function()
{
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		bgBlazon = $(prefix + ".bg-blazon"),
		magnet = $(prefix + ".magnet");
	
	audio[0] = new Audio("audio/s11.mp3");
	
	fadeNavsOut();
	fadeLauncherIn();
	magnet.fadeOut(0);
	
	startButtonListener = function(){
		audio[0].play();
		timeout[1] = setTimeout(function(){
			bgBlazon.fadeOut(500);
			magnet.fadeIn(1000);
		}, 4000);
		timeout[2] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(9);
		}, 25000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(9);
	}, 2000);
}

launch["frame-110"] = function()
{
		theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		question = $(prefix + ".question"),
		answer = $(prefix + ".answer"),
		bulb = $(prefix + ".bulb"),
		mendeleevTable = $(prefix + ".mendeleev-table"),
		checkButton = $(prefix + ".check-button");
	
	typingText = new TypingText(question, 50, true, function(){
		answer.fadeIn(500);
	});
	
	var bulbListener = function(){
		mendeleevTable.toggleClass("mendeleev-table-hidden");
	};
	bulb.off("click", bulbListener);
	bulb.on("click", bulbListener);
	
	var answerListener = function(){
		currField = $(this);
		var exp = ["0","1","2","3","4","5","6","7","8","9","."];
		var currValue = currField.val();
		
		if(matches(currValue.charAt(currValue.length - 1), exp))
		{
			$(prefix + "." + currField.attr("data-key")).html(currValue);
		}
		else
		{
			var currValueArray = currValue.split("");
			currValueArray.pop();
			currValue = currValueArray.join("");
			currField.val(currValue);
		}
		
		if(answer.val() !== "")
			checkButton.fadeIn(300);
	};
	answer.off("keyup", answerListener);
	answer.on("keyup", answerListener);
	
	var checkButtonListener = function(){
		if(answer.val() === answer.attr("data-correct"))
			answer.css("background-color", "#23b23b");
		else
			answer.css("background-color", "#FF0100");
		
		timeout[0] = setTimeout(function(){
			answer.val(answer.attr("data-correct"));
			answer.css("color", "green");
			answer.css("background-color", "");
		}, 3000);
		
		timeout[1] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(10);
		}, 7000);
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	question.fadeOut(0);
	answer.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	checkButton.fadeOut(0);
	answer.val("");
	
	startButtonListener = function(){
		question.fadeIn(500);
		typingText.write();
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(10);
	}, 2000);
}

launch["frame-111"] = function()
{
		theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		elems = $(prefix + ".elem"),
		box = $(prefix + ".box"),
		alo = $(prefix + ".alo"),
		al2o3 = $(prefix + ".al2o3"),
		bauxites = $(prefix + ".bauxites"),
		sprite1 = $(prefix + ".sprite-1"),
		sprite2 = $(prefix + ".sprite-2"),
		sprite3 = $(prefix + ".sprite-3"),
		sprite4 = $(prefix + ".sprite-4"),
		romeII = $(prefix + ".rome-II"),
		romeIII = $(prefix + ".rome-III");
	
	audio[0] = new Audio("audio/s13.mp3");
	
	sprite[0] = new Motio(sprite1[0], {
		"fps": "3",
		"frames": "12"
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	sprite[1] = new Motio(sprite2[0], {
		"fps": "3",
		"frames": "10"
	});
	sprite[1].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	sprite[2] = new Motio(sprite3[0], {
		"fps": "3",
		"frames": "8"
	});
	sprite[2].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	sprite[3] = new Motio(sprite4[0], {
		"fps": "3",
		"frames": "6"
	});
	sprite[3].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	elems.fadeOut(0);
	box.fadeOut(0);
	
	startButtonListener = function(){
		audio[0].play();
		timeout[1] = setTimeout(function(){
			bauxites.fadeIn(500);
		}, 7000);
		timeout[2] = setTimeout(function(){
			bauxites.addClass("seen");
		}, 9000);
		timeout[3] = setTimeout(function(){
			bauxites.removeClass("seen");
		}, 28000);
		timeout[4] = setTimeout(function(){
			box.fadeIn(200);
			alo.fadeIn(500);
		}, 30000);
		timeout[5] = setTimeout(function(){
			romeIII.fadeIn(500);
		}, 39000);
		timeout[6] = setTimeout(function(){
			romeII.fadeIn(500);
		}, 43000);
		timeout[7] = setTimeout(function(){
			sprite1.fadeIn(500);
			sprite[0].play();
		}, 50000);
		timeout[8] = setTimeout(function(){
			//sprite1.fadeOut(500);
			sprite2.fadeIn(500);
			sprite[1].play();
		}, 55000);
		timeout[9] = setTimeout(function(){
			//sprite2.fadeOut(500);
			sprite3.fadeIn(500);
			sprite[2].play();
		}, 60000);
		timeout[10] = setTimeout(function(){
			sprite4.fadeIn(500);
			sprite[3].play();
		}, 65000);
		timeout[11] = setTimeout(function(){
			sprite1.fadeOut(500);
			sprite2.fadeOut(500);
			sprite3.fadeOut(500);
			sprite4.fadeOut(500);
			romeII.fadeOut(500);
			romeIII.fadeOut(500);
			alo.fadeOut(500);
			al2o3.fadeIn(500);
		}, 68000);
		timeout[12] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(11);
		}, 75000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(11);
	}, 2000);
}

launch["frame-112"] = function()
{
		theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		ams = $(prefix + ".am"),
		amup = $(prefix + ".am-up"),
		amdown = $(prefix + ".am-down"),
		checkButton = $(prefix + ".check-button"),
		award = $(prefix + ".award"),
		correctNum = 0;
	
	var areDirty = function()
	{
		for(var i = 0; i < ams.length; i++)
			if($(ams[i]).html() === "0")
				return false;
			
		return true;
	}
	
	var amupListener = function(){
		var target = $(prefix + "." + $(this).attr("data-element")),
			targetValue;
			
		if(target.html() === "")
			targetValue = 1;
		else
			targetValue = parseInt(target.html());
		
		target.html(++targetValue);
	};
	amup.off("click", amupListener);
	amup.on("click", amupListener);
	
	var amdownListener = function(){
		var target = $(prefix + "." + $(this).attr("data-element")),
			targetValue = parseInt(target.html());
		
		if(target.html() === "")
			targetValue = 1;
		else
			targetValue = parseInt(target.html());
		
		if(targetValue >= 2)
			target.html(--targetValue);
		
		if(targetValue === 1)
			target.html("");
	};
	amdown.off("click", amdownListener);
	amdown.on("click", amdownListener);
	
	var checkButtonListener = function(){
		for(var i = 0; i < ams.length; i++)
		{
			var currfield = $(ams[i]);
			if(currfield.html() === currfield.attr("data-correct"))
			{
				currfield.css("background-color", "#23b23b");
				correctNum++;
			}
			else
				currfield.css("background-color", "#FF0100");
		}
		timeout[0] = setTimeout(function(){
			for(var i = 0; i < ams.length; i++)
			{
				var currfield = $(ams[i]);
				currfield.html(currfield.attr("data-correct"));
				currfield.css("background-color", "");
				currfield.css("color", "green");
			}
		}, 3000);
		timeout[1] = setTimeout(function(){
			if(correctNum === 8)
			{
				award.fadeIn(1000);
				blazons.fadeIn(500);
				blazonPlusOne("kostanai");
			}
		}, 5000);
		timeout[2] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(12);
		}, 7000);
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	//checkButton.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	award.fadeOut(0);
	
	startButtonListener = function(){
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(12);
	}, 2000);
}

launch["frame-113"] = function()
{
		theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		helicopter = $(prefix + ".helicopter"),
		mapHighlighted = $(prefix + ".map-highlighted"),
		vocabulary = $(prefix + ".vocabulary"),
		isotope = $(prefix + ".isotope"),
		atomicMass = $(prefix + ".atomic-mass"),
		molarMass = $(prefix + ".molar-mass"),
		ammonia = $(prefix + ".ammonia"),
		glass = $(prefix + ".glass");
	
	audio[0] = new Audio("audio/helicopter-sound.mp3");
	audio[1] = new Audio("audio/s15.mp3");
	audio[2] = new Audio("audio/s3.mp3");
	
	sprite[0] = new Motio(helicopter[0], {
		"fps": "3", 
		"frames": "11"
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			helicopter.fadeOut(1000);
		}
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	mapHighlighted.fadeOut(0);
	helicopter.fadeOut(0);
		
	startButtonListener = function(){
		audio[1].play();
		timeout[1] = setTimeout(function(){
			mapHighlighted.fadeIn(1000);
		}, 1000);
		timeout[2] = setTimeout(function(){
			helicopter.fadeIn(1000);
			audio[0].play();
			audio[2].play();
		}, 3000);
		timeout[3] = setTimeout(function(){
			sprite[0].play();
		}, 4000);
		timeout[4] = setTimeout(function(){
			isotope.removeClass("out-of-view");
		}, 6000);
		timeout[5] = setTimeout(function(){
			atomicMass.removeClass("out-of-view");
		}, 7000);
		timeout[6] = setTimeout(function(){
			molarMass.removeClass("out-of-view");
		}, 16000);
		timeout[7] = setTimeout(function(){
			glass.removeClass("out-of-view");
		}, 23000);
		timeout[8] = setTimeout(function(){
			ammonia.removeClass("out-of-view");
		}, 26000);
		timeout[9] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(13);
		}, 34000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(13);
	}, 2000);
}

launch["frame-114"] = function()
{
		theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"), 
		copper = $(prefix + ".copper"),
		moleculas = $(prefix + ".moleculas"),
		mendeleevTable = $(prefix + ".mendeleev-table");
	
	audio[0] = new Audio("audio/s16.mp3");
	
	sprite[0] = new Motio(moleculas[0], {
		"fps": "1",
		"frames": "12"
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	copper.fadeOut(0);
	mendeleevTable.fadeOut(0);
	
	startButtonListener = function(){
		audio[0].play();
		timeout[1] = setTimeout(function(){
			copper.fadeIn(1000);
		}, 10000);
		timeout[2] = setTimeout(function(){
			mendeleevTable.fadeIn(1000);
		}, 22000);
		timeout[3] = setTimeout(function(){
			sprite[0].play();
		}, 49000);
		timeout[4] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(14);
		}, 63000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(14);
	}, 2000);
}

launch["frame-115"] = function()
{
		theFrame = $("#frame-115"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		pics = $(prefix + ".pics"),
		answers = $(prefix + ".answer"),
		checkButton = $(prefix + ".check-button"),
		award = $(prefix + ".award"),
		correctNum = 0;
	
	var areDirty = function()
	{
		for (var i = 0; i < answers.length; i++)
			if($(answers[i]).val() === "")
				return false;
			
		return true;
	}
	
	var answersListener = function(){
		currField = $(this);
		var exp = ["0","1","2","3","4","5","6","7","8","9","."];
		var currValue = currField.val();
		
		if(matches(currValue.charAt(currValue.length - 1), exp))
		{
			$(prefix + "." + currField.attr("data-key")).html(currValue);
		}
		else
		{
			var currValueArray = currValue.split("");
			currValueArray.pop();
			currValue = currValueArray.join("");
			currField.val(currValue);
		}
		
		if(areDirty())
			checkButton.fadeIn(300);
	};
	answers.off("keyup", answersListener);
	answers.on("keyup", answersListener);
	
	var checkButtonListener = function(){
		for(var i = 0; i < answers.length; i++)
		{
			var currfield = $(answers[i]);
			if(currfield.val() === currfield.attr("data-correct"))
			{
				currfield.css("background-color", "#23b23b");
				correctNum++;
			}
			else
				currfield.css("background-color", "#FF0100");
		}
		timeout[0] = setTimeout(function(){
			for(var i = 0; i < answers.length; i++)
			{
				var currfield = $(answers[i]);
				currfield.val(currfield.attr("data-correct"));
				currfield.css("background-color", "");
				currfield.css("color", "green");
			}
		}, 3000);
		timeout[1] = setTimeout(function(){
			if(correctNum === 7)
			{
				blazons.fadeIn(500);
				blazonPlusOne("kostanai");
				award.fadeIn(1000);
				timeout[0] = setTimeout(function(){
					blazons.fadeOut(500);
					award.html("<br> Заработано эмблем: <br><br>Актюбинской области: <b>"+ blazonAward["aktobe"] + "</b><br>Костанайской области: <b>" + blazonAward["kostanai"] + "</b><br>Восточно-Казахстанской области: <b>" + blazonAward["east-kazakhstan"] + "</b><br>Карагандинской области: <b>" + blazonAward["karaganda"]);
				}, 4000);
			}
			else
			{
				award.html("<br> Заработано эмблем: <br><br>Актюбинской области: <b>"+ blazonAward["aktobe"] + "</b><br>Костанайской области: <b>" + blazonAward["kostanai"] + "</b><br>Восточно-Казахстанской области: <b>" + blazonAward["east-kazakhstan"] + "</b><br>Карагандинской области: <b>" + blazonAward["karaganda"]);
				award.fadeIn(1000);
			}
		}, 5000);
		timeout[3] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(15);
		}, 15000);
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
		
	fadeNavsOut();
	fadeLauncherIn();
	checkButton.fadeOut(0);
	answers.val("");
	award.fadeOut(0);
	pics.fadeOut(0);
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			pics.fadeIn(1000);
		}, 1000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(15);
	}, 2000);
}

launch["frame-201"] = function()
{
		theFrame = $("#frame-201"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		logo = $(prefix + ".logo");
	
	audio[0] = new Audio("audio/game-theme.mp3");
	
	fadeNavsOut();
	fadeLauncherIn();
	
	logo.fadeOut(0);
		
	startButtonListener = function(){
		audio[0].play();
		timeout[1] = setTimeout(function(){
			bg.fadeIn(500);
		}, 500);
		timeout[2] = setTimeout(function(){
			logo.fadeIn(500);
		}, 1000);
		timeout[3] = setTimeout(function(){
			logo.addClass("state-1");
		}, 2000);
		timeout[4] = setTimeout(function(){
			logo.addClass("state-2");
		}, 4000);
		timeout[4] = setTimeout(function(){
			hideEverythingBut($("#frame-202"));
		}, 8000);
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(15);
	}, 2000);
}

launch["frame-202"] = function()
{
		theFrame = $("#frame-202"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		tour = $(prefix + ".tour"),
		correctNumber = $(prefix + ".correct-number"),
		wheel = $(prefix + ".wheel"),
		rotateButton = $(prefix + ".rotate-button"),
		helpButton = $(prefix + ".help-button"),
		inputs = $(prefix + "input"), 
		questions = $(prefix + ".question"),
		tables = $(prefix + ".table"),
		activeTour = 0, 
		check = $(prefix + ".check"),
		hint1 = $(prefix + ".hint-1"),
		formulas = $(prefix + ".formula"), 
		mistakesNum, added = false;
	
	audio[0] = new Audio("audio/wheel-spin.mp3");
	
	sprite[0] = new Motio(wheel[0], {
		"fps": "3", 
		"frames": "3"
	});
	
	var wordIsGuessed = function(){
		var currInputs = $(prefix + ".table-" + activeTour + " .cell");
		for(var i = 0; i < currInputs.length; i++)
		{
			var currInput = $(currInputs[i]);
			
			if(currInput.val() != currInput.attr("data-correct"))
				return false
		}
		return true;
	};
	
	var rotateButtonListener = function(){
		mistakesNum = 0;
		hint1.fadeOut(500);
		sprite[0].play();
		audio[0].play();
		activeTour++;
		questions.fadeOut(0);
		tables.fadeOut(0);
		formulas.fadeOut(0);
		helpButton.fadeOut(0);
		check.fadeOut(0);
		rotateButton.fadeOut(0);
		added = false;
		
		timeout[0] = setTimeout(function(){
			sprite[0].pause();
			sprite[0].to(activeTour % 3 - 1, true);
			tour.html("тур №" + activeTour);
			$(prefix + ".question-" + activeTour).fadeIn(1000);
			$(prefix + ".table-" + activeTour).fadeIn(1000);
			audio[0].pause();
		}, 5000);
	};
	rotateButton.off("click", rotateButtonListener);
	rotateButton.on("click", rotateButtonListener);
	
	var inputsListener = function(){
		var currInput = $(this);
		currInput.css("color", "white");
		
		if(currInput.val() === currInput.attr("data-correct"))
		{
			currInput.unbind("click");
			currInput.css("background-color", "white");
			currInput.css("color", "black");
			currInput.attr("readonly", true);
			
			var nextInput = currInput.next();
			
			while(nextInput.length && nextInput.attr("class") !== "cell")
			{
				nextInput = nextInput.next();
			}
			
			nextInput.focus();
			
			if(wordIsGuessed())
			{
				nextInput.blur();
				
				$(prefix + ".formula-" + activeTour).fadeIn(500);
				check.fadeIn(500);
					
				var points;
				
				if(activeTour === 1 || activeTour === 4)
					points = 100;
				else if(activeTour === 2)
					points = 20;
				else if(activeTour === 3)
					points = 50;
				
				if(!added)
				{
					points = points + parseInt(correctNumber.html());
					correctNumber.html(points);
					added = true;
					
					if(activeTour < 4)
					{
						hint1.fadeIn(500);
						rotateButton.fadeIn(0);
					}
					else
					{
						timeout[0] = setTimeout(function(){
							correctNumber.addClass("correct-number-final");
							correctNumber.append(" баллов");
							fadeHomeIn();
							finished = true;
						}, 2000)
					}	
				}				
			}
		}
		else
		{
			currInput.val("");
			mistakesNum++;
			
			if(mistakesNum >= 3 && activeTour !== 4)
				helpButton.fadeIn(500);
		}		
	};
	inputs.bind("keyup", inputsListener);
	
	helpButtonListener = function(){
		currInputs = $(prefix + ".table-" + activeTour + " .cell");
		
		for(var i = 0; i < currInputs.length; i += 4)
		{
			currInput = $(currInputs[i]);
			currInput.val(currInput.attr("data-correct"));
			currInput.css("background-color", "white");
			currInput.css("color", "black");
		}
		
		if(wordIsGuessed())
		{			
			$(prefix + ".formula-" + activeTour).fadeIn(500);
			check.fadeIn(500);
				
			var points;
			
			if(activeTour === 1 || activeTour === 4)
				points = 100;
			else if(activeTour === 2)
				points = 20;
			else if(activeTour === 3)
				points = 50;
			
			if(!added)
			{
				points = points + parseInt(correctNumber.html());
				correctNumber.html(points);
				added = true;
				
				if(activeTour < 4)
				{
					hint1.fadeIn(500);
					rotateButton.fadeIn(0);
				}
				else
				{
					timeout[0] = setTimeout(function(){
						correctNumber.addClass("correct-number-final");
						correctNumber.append(" баллов");
						fadeHomeIn();
						finished = true;
					}, 2000)
				}		
			}			
		}
	};
	helpButton.off("click", helpButtonListener);
	helpButton.on("click", helpButtonListener);
	
	inputs.val("");
	questions.fadeOut(0);
	tables.fadeOut(0);
	check.fadeOut(0);	
	helpButton.fadeOut(0);
	formulas.fadeOut(0);
	
	startButtonListener = function(){
	}
}

launch["frame-301"] = function()
{
		theFrame = $("#frame-301"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		bg = $(prefix + ".bg"),
		symbols = $(prefix + ".symbols"),
		box = $(prefix + ".box");
	
	startButtonListener = function(){
	};
}

var hideEverythingBut = function(elem)
{
	if(theFrame)
		theFrame.html(theClone.html());
	
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	elemId = elem.attr("id");
	regBox = $(".reg-box");
	fadeTimerOut();
	blazons.fadeOut(0);
	
	if(!globalName)
		regBox.fadeOut(0);
	
	if(elemId === "frame-000")
	{
		fadeNavsOut();
		fadeTimerOut();
	}
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	

	for (var i = 0; i < timeout.length; i++)
	{
		clearTimeout(timeout[i]);
	}
	
	for (var i = 0; i < sprite.length; i++)
		sprite[i].pause();
	
	if(typingText)
		typingText.stopWriting();
	
	if(elem.attr("id") === "frame-000")
		regBox.fadeIn(0);
	
	launch[elemId]();
	initMenuButtons();
	
	if(elem.hasClass("fact"))
		$(".o2").fadeOut(0);
	
	if(elem.attr("id") === "frame-000")
	{
		$(".o2").fadeOut(0);
		fadeNavsOut();
	}
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	var video = $(".intro-video"),
		pic = $(".intro-pic");
	
	initMenuButtons();
	setLRSData();
	blazons = $(".blazon-award");
	hideEverythingBut($("#frame-000"));
	setMenuStuff();
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	video[0].play();
	
	timeout[0] = setTimeout(function(){
		video.hide();
	}, 10000);
	timeout[1] = setTimeout(function(){
		pic.hide(); 
	}, 15000);
};

$(document).ready(main);